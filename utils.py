import glob
import json
import os
import logging
import random
import re
import string
from pathlib import Path
from threading import Thread

addslashes = "addslashes"
S_PREFIXE = "pref"
S_SUFFIXE = "suf"
S_SAFETY = "safety"
S_CODE = "code"
magic_quotes_filter = "magic_quotes_filter"
FOLDER_UNSAFE = "unsafe/"
FOLDER_SAFE = "safe/"

set_js_variable = ["http_var",
                   "$cmd",
                   "$script",
                   "$child",
                   "$fs_var",
                   "$filename",
                   "url_var",
                   "$req",
                   "q_var",
                   "$cp",
                   "$buff",
                   "$execFile",
                   "$temp",
                   "Input", #class
                   "$input",
                   "safe",
                   "$test",
                   "$realOne",
                   "$trap",
                   "$array",
                   "str_var",
                   "$re",
                   "$email_re",
                   "email_filter", #function name
                   "magic_quotes_filter", #function name
                   "addslashes", #function name
                   "full_special_chars_filter", #function name,
                   "$map_var",
                   "special_chars_filter", #function name
                   "number_float_filter", #functio name
                   "$all_except_number_re",
                   "$numbers_re",
                   "$float_re",
                   "$validation_float",
                   "$integer_replace",
                   "$integer_validation",
                   "htmlEntities", #function name
                   "func_htmlspecialchars", #function name
                   "$legal_table",
                   "server",
                   "res_var",
                   "$m",
                   #HOP extension
                   "func_var",
                   "script_var",
                   "body_var",
                   "head_var",
                   "div_var",
                   "style_var",
                   "cmd_var",
                   "error_var",
                   "stdout_var",
                   "stderr_var",
                   "$cp",
                   "$cat_var",
                   "$tainted_var",
                   "data_var",
                   "code_var",
                   "param_var",
                   "spawnLaunchScript", #function name
                   "execShellCommand", #function name
                   "readfile_func", #function name
                   "forkLaunchScript", #function name
                   "file_var",
                   "$compute_var",
                    #NODEJS WITH SANITIZATION ON REQUIRE
                    "$char_replace",
                   "$re_null",
                   "$func_filter",
                   "$char_replace_null",
                   "email_filter"


                   ]
def create_folder_if_not_exist(path) :

    if not os.path.exists(path) :
        os.makedirs(path, exist_ok=True)
        logging.info("%s doesn't exist ==> creation of this folder",path)
    else :
         logging.info("%s exist ==> do nothing",path)


def add_sanitization_to(sanitization, pathfile):
    pattern = re.compile("\$_GET\[(\w|\W|\s)*\]")
    pattern = re.compile("\$_(GET|POST)\[(\w|\W|\s)*\]")
    res = ""
    for i, line in enumerate(open(pathfile)):
        logging.debug(line)
        for match in re.finditer(pattern, line):
            logging.info('Found on line %s: %s' % (i + 1, match.group()))
            line = line.replace(match.group(), sanitization[S_PREFIXE] + match.group() + sanitization[S_SUFFIXE])
        res += line
    logging.info("The result file is ===> %s\n", res)
    return res

def removeFlawComment(pathfile):
    pattern = re.compile("(\$tainted|\$sanitized|userData|UserData|userdata|Userdata)")
    pattern = re.compile("\/\/flaw")
    res = ""
    for i, line in enumerate(open(pathfile)):
        logging.debug(line)
        for match in re.finditer(pattern, line):
            logging.info('Found on line %s: %s' % (i + 1, match.group()))
            line = line.replace(match.group(), "")
        res += line
    logging.debug("The result file is ===> %s\n", res)
    return res

def renameQueriesIn(array_string_code) :
    pattern = re.compile("(userData|UserData|userdata|Userdata)")
    new_query_name = get_random_alphanumeric_string(random.randint(1,20))
    res = []
    for i, line in enumerate(array_string_code):
        logging.debug(line)
        for match in re.finditer(pattern, line):
            logging.info('Found on line %s: %s' % (i + 1, match.group()))
            line = line.replace(match.group(), new_query_name)
        res.append(line)
    logging.debug("The result file is ===> %s\n", res)
    return res

def removeFlawCommentIn(array_string_code):
    pattern = re.compile("\/\/flaw")
    res = []
    for i, line in enumerate(array_string_code):
        logging.debug(line)
        for match in re.finditer(pattern, line):
            logging.info('Found on line %s: %s' % (i + 1, match.group()))
            line = line.replace(match.group(), "")
        res.append(line)
    logging.debug("The result file is ===> %s\n", res)
    return res

def renameVariableName(newstring, pathfile):
    pattern = re.compile("(\$tainted|\$sanitized|userData|UserData|userdata|Userdata)")
    pattern = re.compile("\$tainted")
    res = ""
    for i, line in enumerate(open(pathfile)):
        logging.debug(line)
        for match in re.finditer(pattern, line):
            logging.info('Found on line %s: %s' % (i + 1, match.group()))
            line = line.replace(match.group(), newstring)
        res += line
    logging.info("The result file is ===> %s\n", res)
    return res

def renameBy(newVariableName, oldVariableName, pathfile):
    pattern = re.compile("(\$tainted|\$sanitized|userData|UserData|userdata|Userdata)")
    pattern = re.compile(oldVariableName)
    res = ""
    for i, line in enumerate(open(pathfile)):
        logging.debug(line)
        for match in re.finditer(pattern, line):
            logging.info('Found on line %s: %s' % (i + 1, match.group()))
            line = line.replace(match.group(), newVariableName)
        res += line
    logging.info("The result file is ===> %s\n", res)
    return res

def renameHTTP(newVariableName, pathfile):
    pattern = re.compile("[^'\"]http")
    res = ""
    for i, line in enumerate(open(pathfile)):
        logging.debug(line)
        for match in re.finditer(pattern, line):
            logging.info('Found on line %s: %s' % (i + 1, match.group()))
            line = line.replace(match.group(), " " + newVariableName)
        res += line
    logging.info("The result file is ===> %s\n", res)
    return res

def renameServer(newVariableName, pathfile):
    pattern = re.compile("[^'\"]server")
    res = ""
    for i, line in enumerate(open(pathfile)):
        logging.debug(line)
        for match in re.finditer(pattern, line):
            logging.info('Found on line %s: %s' % (i + 1, match.group()))
            line = line.replace(match.group(), " " + newVariableName)
        res += line
    logging.info("The result file is ===> %s\n", res)
    return res

def renameSpecificJsVariable(newVariableName, oldVariableName, pathfile):
    pre_oldVariableName = addBackSlashes(oldVariableName)
    logging.debug("dollars? %s", pre_oldVariableName)
    pattern = re.compile(pre_oldVariableName)
    res = ""
    for i, line in enumerate(open(pathfile)):
        logging.debug(line)
        for match in re.finditer(pattern, line):
            logging.info('Found on line %s: %s' % (i + 1, match.group()))
            line = line.replace(match.group(), newVariableName)
        res += line
    logging.info("The result file is ===> %s\n", res)
    return res

def renameSpecificJsVariableBy(newVariableName, oldVariableName, array_line_code):
    pre_oldVariableName = addBackSlashes(oldVariableName)
    logging.debug("dollars? %s", pre_oldVariableName)
    pattern = re.compile(pre_oldVariableName)
    res = []
    for i, line in enumerate(array_line_code):
        logging.debug(line)
        for match in re.finditer(pattern, line):
            logging.info('Found on line %s: %s' % (i + 1, match.group()))
            line = line.replace(match.group(), newVariableName)
        res.append(line)
    logging.info("The result file is ===> %s\n", res)
    return res

def renameVariableBy(newVariableName, oldVariableName, array_code_lines, exception_list=False):
    process_oldVariableName = addBackSlashes(oldVariableName)
    logging.debug("%s became %s", oldVariableName, process_oldVariableName)
    pattern = re.compile(process_oldVariableName)
    res = []
    for i, line in enumerate(array_code_lines):
        logging.debug(line)
        for match in re.finditer(pattern, line):
            logging.info('Found on line %s: %s' % (i + 1, match.group()))
            if not exception_list :
                line = line.replace(match.group(), newVariableName)
            else :
                if match.group() in exception_list :
                    logging.info('EXCEPTION Found on line %s: %s' % (i + 1, match.group()))
                    line = line
                else :
                    line = line.replace(match.group(), newVariableName)
        res.append(line)
    # logging.debug("The result file is ===> %s\n", res)
    return res

def renameAllVariableForAFile(pathfile, exception_list=False) :
    l_variables = getListVariables()
    l_variables_extended = l_variables + set_js_variable
    logging.info(l_variables_extended)
    string_code = open(pathfile)
    for variable in l_variables_extended :
        newVariable = get_random_alphanumeric_string(random.randint(3,20))
        res = renameVariableBy(newVariable, variable, string_code, exception_list=exception_list)
        string_code = res
    return res

def createFolderForNewDataset(pathname, datasetname) :
    folder_safe = "safe"
    folder_unsafe = "unsafe"
    create_folder_if_not_exist(os.path.join(pathname, datasetname, folder_safe))
    create_folder_if_not_exist(os.path.join(pathname, datasetname, folder_unsafe))


def get_random_alphanumeric_string_with_counts(letters_count, digits_count):
    sample_str = ''.join((random.choice(string.ascii_letters) for i in range(letters_count)))
    sample_str += ''.join((random.choice(string.digits) for i in range(digits_count)))

    # Convert string to list and shuffle it to mix letters and digits
    sample_list = list(sample_str)
    random.shuffle(sample_list)
    final_string = ''.join(sample_list)
    return final_string

def get_random_alphanumeric_string(length):
    letters = string.ascii_letters
    prefix_str = ''.join((random.choice(letters) for i in range(random.randint(1,20))))
    letters_and_digits = string.ascii_letters + string.digits
    result_str = ''.join((random.choice(letters_and_digits) for i in range(length)))
    res = prefix_str + result_str
    logging.debug("Random alphanumeric String is: %s", res)
    return res

def getListVariables() :

    path_json = 'variable.json'
    with open(path_json) as json_file:
        data = json.load(json_file) #1 : key : php variable - value : hash value // #2 key : hash value - value : php variable
    dict_key_variable = data[1]
    res_list = list(dict(dict_key_variable).keys())
    pre_res_list = map(addDollars, res_list)
    return list(pre_res_list)


def addBackSlashes(string):
    res = str(string)
    if res.__contains__("$") :
        return "\\" + string
    else :
        return string


def getListOfVariablesPattern() :
    pre_list = getListVariables()
    return list(map(addBackSlashes, pre_list))

def addDollars(string) :
    return "$" + string

def write_on(pathfile, string):
    f = open(pathfile, "w")
    f.write(string)
    f.close()

def testSafety(construction, sanitize) :
    #########################################################################
    # Special case (Order mater !!!!)

    # Ok in every case
    # No flow between source to sink as if the construction is unsafe make the sample safe
    # always precise that they is no flow on the xml because the sample will be classify like unsafe
    if isOn(construction, "unsafe") == "1" and isOn(sanitize, "noFlow") == 1:
        logging.debug("unsafe + noflow ")
        return 1

    # Ok in every case
    # Universal unsafe contrustuction
    # always precise that they is no flow on the xml because with this condition the sample will be classify like unsafe
    if isOn(construction, "unsafe") == 1:
        logging.debug("unsafe by construction")
        return 0

    # Universal safe sanitizing (Cast & co)
    # Ok in every case
    # always precise if they is no flow on the xml because the sample will be classify like safe
    if isOn(sanitize, "safe") == "1" and isOn(sanitize, "noFlow") == 1:
        logging.debug("safe + noflow ")
        return 1

    if isOn(sanitize, "noFlow") == 1:
        logging.debug("noflow ")
        return 1

    # Universal safe sanitizing (Cast & co)
    # Ok in every case as if they is a flow between source and sink
    if isOn(sanitize, "safe") == 1:
        logging.debug("safe")
        return 1

    if isOn(sanitize, "onlyAlphaNumericChar") == 1:
        logging.debug("onlyAlphaNumericChar")
        return 1

    # TODO : replace by test on the format
    # 3 :Escaping (" -> \" ) with rule 3 can lead to XSS (HTML parser first... blablabla... Read the OWASP doc thx)
    # Only used for 1 sanitization called $tainted = mysql_real_escape_string($tainted); -- it does'nt exist anymore on PHP 7
    if isOn(construction, "rule") == 3 and isOn(sanitize, "escape") == 1:
        logging.debug("rule 3 + escape ")
        return 0

    # 3 :On script or eventHandler context, percent encoding can lead XSS
    if isOn(construction, "rule") == 3 and isOn(sanitize,"percentEncodingAllowed") ==0:
        logging.debug("rule 3 + percentEncodingAllowed ")
        return 0

        # 5 :WARNING: Do not encode complete or relative URL's with URL encoding!
    # If untrusted input is meant to be placed into href, src or other URL-based attributes,
    # it should be validated to make sure it does not point to an unexpected protocol
    if isOn(construction, "rule") == 5 and isOn(sanitize,"urlValidated") ==0:
        logging.debug("rule 3 + urlValidated ")
        return 0

    # 3 :Escape </script> tag needed (Rule 3)
    if isOn(construction,"scriptBlock") ==1 and isOn(sanitize,"angleBrackets") ==0:
        logging.debug("scriptBlock + angleBrackets ")
        return 0

    # 4 :Escape </style> tag needed (Rule 4)
    if isOn(construction,"styleBlock") ==1 and isOn(sanitize,"angleBrackets") ==0:
        logging.debug("styleBlock + angleBrackets ")
        return 0

    # 4 : URL must NOT start with "javascript" (but with http) (in CSS)
    # Not use by the construction.xml
    if isOn(construction,"URL_CSS_context") ==1 and isOn(sanitize,"URL_CSS_context") ==0:
        logging.debug("URL_CSS_context + URL_CSS_context ")
        return 0

    # 4 : URL must NOT start with "javascript" (but with http) (in HTML)
    if isOn(construction,"URL_HTML_context") ==1 and isOn(sanitize,"URL_HTML_context") ==0:
        logging.debug("URL_HTML_context + URL_HTML_context ")
        return 0

    # 4 : must NOT start with "javascript:"
    if isOn(construction,"eventHandler") ==1 and isOn(sanitize,"eventHandler") ==0:
        logging.debug("eventHandler + eventHandler ")
        return 0

    # 4 : property must NOT start with "expression" (in CSS)
    # Not use by the construction.xml
    # doesn't work on IE11 and Edge44
    # work on IE6/7
    # expression property is not compatible with W3C specification
    if isOn(construction,"property_CSS_context") ==1 and isOn(sanitize,
        "property_CSS_context") ==0:
        logging.debug("property_CSS_context + property_CSS_context ")
        return 0

    # 3 :anti-slash can break quotes on scriptBlock or styleBlock can lead XSS
    if isOn(construction, "rule") == 3 and isOn(sanitize,"antiSlashAllowed") ==0:
        logging.debug("rule 3 + antiSlashAllowed ")
        return 0

    # 4 :anti-slash can break quotes on scriptBlock or styleBlock can lead XSS
    if isOn(construction, "rule") == 4 and isOn(sanitize,"antiSlashAllowed") ==0:
        logging.debug("rule 4 + antiSlashAllowed ")
        return 0

    # Simple quote escape is enough (Rule 2,3,4) BUT Escape </script> tag needed (Rule 3)
    if isOn(construction,"simpleQuote") ==1 and isOn(sanitize,"angleBrackets") ==0:
        logging.debug("simpleQuote + angleBrackets ")
        return 0

    # double quote escape is enough (Rule 2,3,4) BUT Escape </script> tag needed (Rule 3)
    if isOn(construction,"doubleQuote") ==1 and isOn(sanitize,"doubleQuote") ==0:
        logging.debug("doubleQuote + doubleQuote ")
        return 0

    # Simple quote escape is enough (Rule 2,3,4)
    if isOn(construction,"simpleQuote") ==1 and isOn(sanitize,"simpleQuote") ==1:
        logging.debug("simpleQuote + simpleQuote ")
        return 1

    # Simple quote are not filtered/flushed/encoded on &#HH; format or HTML entities (Rule 2,3,4)
    if isOn(construction,"simpleQuote") ==1 and isOn(sanitize,"simpleQuote") ==0:
        logging.debug("simpleQuote + NO simpleQuote ")
        return 0

    # Double quote escape is enough (Rule 2,3,4)
    if isOn(construction,"doubleQuote") ==1 and isOn(sanitize,"doubleQuote") ==1:
        logging.debug("doubleQuote + doubleQuote ")
        return 1

    # Double quote are not filtered/flushed/encoded on &#HH; format or HTML entities (Rule 2,3,4)
    if isOn(construction,"doubleQuote") ==1 and isOn(sanitize,"doubleQuote") ==0:
        logging.debug("doubleQuote + NO doubleQuote ")
        return 0

    #########################################################################
    # General Case
    # Rule 0 : The first rule is to deny all -
    # don't put untrusted data into your HTML document unless it is within one of the slots defined in Rule #1 through Rule #5
    if isOn(construction, "rule") == 0 and isOn(sanitize,"unQuote") ==1 \
            and isOn(sanitize,"onlyNumericChar") ==1 and isOn(sanitize,"minusAllowed") ==1 \
            and isOn(sanitize,"addAllowed") ==1 and isOn(sanitize,"dotAllowed") ==1 and \
            isOn(sanitize,"addToSpaceFlushed") ==1:
        logging.debug("rule == 0 + onlyNumericChar ")
        return 1

    # Rule 1 : escape & < > " '     (and / ideally)
    if isOn(construction, "rule") == 1 and isOn(sanitize,"rule1") ==1:
        logging.debug("rule == 1  + rule == 1  ")
        return 1

    # Rule 2 : escape ASCII < 256 (format : &#xHH ) ideally
    # properly quoted attribute only need corresponding quote
    if isOn(construction, "rule") == 2 and isOn(sanitize,"rule2") ==1:
        logging.debug("rule == 2 + rule == 2 ")
        return 1

    # Rule 2 : escape ASCII < 256 (format : &#xHH ) ideally
    # Unquoted attributes can NOT be broken out of with many characters because if they are filtered/flushed/encoded on &#HH; format or HTML entities, including [space] % * + , - / ; < = > ^ and |
    if isOn(construction, "rule") == 2 and isOn(sanitize,"unQuote") ==1 \
            and isOn(sanitize,"onlyNumericChar") ==1 and isOn(sanitize,"minusAllowed") ==1 \
            and isOn(sanitize,"addAllowed") ==1 and isOn(sanitize,"dotAllowed") ==1 and \
            isOn(sanitize,"addToSpaceFlushed") ==1:
        logging.debug("rule == 2 + onlyNumericChar ")
        return 1

    # Rule 3 : escape ASCII < 256 (format : \xHH)
    # properly quoted attribute only need corresponding quote
    # </script> can still close a script block
    if isOn(construction, "rule") == 3 and isOn(sanitize,"rule3") ==1:
        logging.debug("rule == 3 + rule == 3 ")
        return 1

    # Rule 4 : escape ASCII < 256 (format : \HH)
    # properly quoted attribute only need corresponding quote
    # + escape </style>
    if isOn(construction, "rule") == 4 and isOn(sanitize,"rule4") ==1:
        logging.debug("rule 4 + rule 4 ")
        return 1

    # Rule 4 : escape ASCII < 256 (format : \HH)
    # properly quoted attribute only need corresponding quote
    # + escape </style>
    # Unquoted attributes can NOT be broken out of with many characters because if they are  filtered/flushed/encoded on &#HH; format or HTML entities, including [space] % * + , - / ; < = > ^ and |
    if isOn(construction, "rule") == 4 and isOn(sanitize,"unQuote") ==1 \
            and isOn(sanitize,"onlyNumericChar") ==1 and isOn(sanitize,"minusAllowed") ==1 \
            and isOn(sanitize,"addAllowed") ==1 and isOn(sanitize,"addToSpaceFlushed") ==1:
        logging.debug("rule 4  + onlyNumericChar ")
        return 1

    # Rule 5 : escape ASCII < 256 (format : %HH)
    # TO double checked, and improve...
    if isOn(construction, "rule") == 5 and isOn(sanitize,"rule5") ==1:
        logging.debug("rule 5 + rule 5 ")
        return 1

    # Rule 5 : escape ASCII < 256 (format : %HH)
    # Unquoted attributes can NOT be broken out of with many characters because if they are  filtered/flushed/encoded on &#HH; format or HTML entities, including [space] % * + , - / ; < = > ^ and |
    if isOn(construction, "rule") == 5 and isOn(sanitize,"unQuote") ==1 \
            and isOn(sanitize,"onlyNumericChar") ==1 and isOn(sanitize,"minusAllowed") ==1 \
            and isOn(sanitize,"addAllowed") ==1 and isOn(sanitize,"addToSpaceFlushed") ==1:
        logging.debug("rule 5 + onlyNumericChar ")
        return 1

    logging.debug("UNSAFE BY DEFAULT ")
    return 0

def isOn(dict, att) :
    try :
        return int(dict[att])
    except KeyError as e :
        return False


def generate_sanitize_file(dict_sanitization, construction, path_file):
    path_file_parts = path_file.parts
    add_to_path = path_file_parts[-2]
    db_name = "real-modified-db"
    create_folder_if_not_exist(os.path.join(db_name, add_to_path,FOLDER_UNSAFE))
    create_folder_if_not_exist(os.path.join(db_name, add_to_path,FOLDER_SAFE))
    for sanitization in dict_sanitization:
        logging.info(sanitization)
        logging.info(dict_sanitization[sanitization])
        safety = testSafety(construction, dict_sanitization[sanitization][S_SAFETY])
        logging.info("Is it safe? %s", safety)

        NAME_RES = os.path.join(db_name, add_to_path, (FOLDER_SAFE if safety else FOLDER_UNSAFE), path_file.stem + "__" + sanitization + ".php")
        logging.info("Name of the new file %s", NAME_RES)
        res_php_code = add_sanitization_to(dict_sanitization[sanitization], path_file)
        write_on(NAME_RES, res_php_code)

COUNT=1
def renameAllVariablesFor(count, filenames, root, safeOrUnsafeFolderName, new_root_dataset_path, exception_list=False):

    for filename in filenames:
        new_filename = "renamedVariable" + str(count) + "__" + filename
        new_filepath = os.path.join(new_root_dataset_path, safeOrUnsafeFolderName, new_filename)
        logging.debug(new_filepath)
        file_path = os.path.join(root, filename)
        array_lines_res = renameAllVariableForAFile(file_path, exception_list=exception_list)
        array_lines_res = removeFlawCommentIn(array_lines_res)
        array_lines_res = renameQueriesIn(array_lines_res)
        string_res = "".join(array_lines_res)
        write_on(new_filepath, string_res)
        count += 1
    return count

def renameAllVariablesFor2(filenames, root, safeOrUnsafeFolderName, new_root_dataset_path):

    for filename in filenames:
        global COUNT
        new_filename = "renamedVariable" + str(COUNT) + "__" + filename
        new_filepath = os.path.join(new_root_dataset_path, safeOrUnsafeFolderName, new_filename)
        logging.debug(new_filepath)
        file_path = os.path.join(root, filename)
        array_lines_res = renameAllVariableForAFile(file_path)
        array_lines_res = removeFlawCommentIn(array_lines_res)
        array_lines_res = renameQueriesIn(array_lines_res)
        string_res = "".join(array_lines_res)
        write_on(new_filepath, string_res)
        COUNT += 1
    return COUNT

class ThreadForRenameAllVariablesFor(Thread):
    """Thread chargé simplement d'afficher une lettre dans la console."""
    def __init__(self, root, is_safe, new_root_dataset_path):
        Thread.__init__(self)
        self.filenames_root = glob.glob(os.path.join(os.path.join(root, "*.*")), recursive=True)
        self.filenames = list(map(lambda filename: Path(filename).name , self.filenames_root))
        logging.info("number of file %s", len(self.filenames))
        self.root = root
        self.is_safe = is_safe
        self.new_root_dataset_path = new_root_dataset_path


    def run(self):
        """Code à exécuter pendant l'exécution du thread."""

        COUNT = renameAllVariablesFor2( self.filenames, self.root, self.is_safe, self.new_root_dataset_path)


def addSpaceIn(string_line) :
    rx_dict = {
        'semi_columns': re.compile(r'(?P<semi_columns>[;,])'),
        'call_function': re.compile(r"(?P<define_service>\w+\()"),
        'close_function': re.compile(r'(?P<close_function>\))'),
        'open_parenthesis': re.compile(r'(?P<close_function>\(w+)'),
        'open_parenthesis': re.compile(r'(?P<close_function>\(w+)'),
    }
    # res = [i for i in re.split(r'(\d+|\W+|\w+)', string_line) if i]
    res = [i for i in re.split(rx_dict['semi_columns'], string_line) if i]
    res = [x.strip(' ') for x in res]
    res = " ".join(res)
    res = [i for i in re.split(rx_dict['call_function'], res) if i]
    res = [x.strip(' ') for x in res]
    res = " ".join(res)
    res = [i for i in re.split(rx_dict['close_function'], res) if i]
    res = [x.strip(' ') for x in res]
    res = " ".join(res)
    res = [i for i in re.split(rx_dict['open_parenthesis'], res) if i]
    res = [x.strip(' ') for x in res]
    res = " ".join(res)
    return res
